require 'rubygems'
require 'sinatra'
use Rack::Logger

configure :production, :development do
  LOGGER = Logger.new("sinatra.log")
  enable :logging, :dumb_errors
  set :raise_errors, true
end

helpers do
  def logger
    LOGGER
  end
end

get '/sms' do
  content_type 'text/xml'
  '<?xml version="1.0" encoding="UTF-8"?>
    <Response>
    </Response>'
end

get '/sms/*' do
  content_type 'text/xml'
  "params = #{params[:splat][:body]}\n\n"
  logger.write "params = #{params[:splat][:body]}\n\n"
end

post '/sms/*' do
  content_type 'text/xml'
  logger.write "params = #{params[:splat][:body]}\n\n"
  '<?xml version="1.0" encoding="UTF-8"?> <Response> </Response>'
end
