require 'rubygems'
require 'twilio-ruby'
require 'phony'
require 'docopt'
require 'psych'
require 'pp'

doc = <<DOCOPT

Testing Docopt

Usage:
  #{__FILE__} <to> <message>
  #{__FILE__} -h | --help
  #{__FILE__} -v | --version


Options:
  -h, --help      Show this screen
  -v, --version   Show version
  -p              Pretty version

DOCOPT

begin
  arguments = Docopt::docopt(doc,:version=>"Awesome 1.00")
  rescue Docopt::Exit => e
    puts e.message
end
  
def numberClean(number)
  if( Phony.plausible?(number,cc: '1') && number.scan(/\d+/).join().length > 10 )
    @to = Phony.normalize(number)
  else
    abort("The send number must be 10 digits with a leading 1")
  end
end

def sendSms()
  auth = Psych.load_file('auth.yaml')
  @client = Twilio::REST::Client.new(auth['account_sid'], auth['auth_token'])
  @account = @client.account
  @message = @account.sms.messages.create({:from => auth['from'], :to => @to, :body => @body})
  puts @message
end

def messageProcess(message)
  if(message.length == 0)
    abort("The message length can not be zero.")
  end
  @body = message
  message.split(/(\W)/)
  body = message.scan(/.{159}/m) 
end


numberClean(arguments["<to>"])
messageProcess(arguments['<message>'])

sendSms()
