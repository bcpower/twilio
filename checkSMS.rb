require 'rubygems'
require 'twilio-ruby'
require 'phony'
require 'psych'
require 'docopt'

doc = <<DOCOPT
Usage:
  #{__FILE__} [-r | --refresh <rate>]
  #{__FILE__} -h | --help
  #{__FILE__} -v | --version


Options:
  -r, --refresh <rate>  Set the refresh rate for checking twilio for messages
  -h, --help            Show this screen
  -v, --version         Show version

DOCOPT

begin
  arguments = Docopt::docopt(doc,:version=>"Version 0.5")
  rescue Docopt::Exit => e
    puts e.message
end
  

auth = Psych.load_file('auth.yaml')

@client = Twilio::REST::Client.new(auth['account_sid'], auth['auth_token'])
@account = @client.account

def getSmsMessages()
  puts Time.now()
  @account.sms.messages.list(:page => 0, :page_size => 5).each do |sms|
    puts " From: #{numberFormat(sms.from)}  To: #{numberFormat(sms.to)} --- #{sms.date_sent}"
    puts "\tBody: #{sms.body }  Price:#{sms.price}"
  end
end

def numberFormat(number)
  num = Phony.split(Phony.normalize(number))
  "(" + num[1] + ")" + num[2] + "-" +num[3]
end

while 1 do 
  #clears screen of old messages
  puts "\e[H\e[2J"

  getSmsMessages()
  sleep 60
end
